package domain;

import domain.AudioMessage;
import domain.FriendSuggester;
import domain.PhotoMessage;
import domain.TextMessage;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class FriendSuggesterTest {

    TextMessage messageMentioningEran = new TextMessage("s","r","eran is holding a flower");
    TextMessage messageNotMentioningEran = new TextMessage("s","r","yaniv is holding a flower");

    FriendSuggester suggester = new FriendSuggester();

    @Test
    public void shouldSuggestBothPartiesIfUserIsMentionedInText(){

        assertEquals(asList("s","r"), suggester.suggest("eran",asList(messageMentioningEran, messageNotMentioningEran)));
    }

    @Test
    public void shouldSuggestBothPartiesIfUserIsMentionedInCaption(){
        PhotoMessage messageMentioningEran = new PhotoMessage("s1","r1","eran is holding a flower", "photo".getBytes());
        PhotoMessage messageNotMentioningEran = new PhotoMessage("s2","r2","yaniv is holding a flower", "photo".getBytes());
        assertEquals(asList("s1","r1"), suggester.suggest("eran", asList(messageMentioningEran, messageNotMentioningEran)));
    }


    @Test
    public void shouldSuggestBothPartiesIfUserIsMentionedInAudio(){
        AudioMessage messageMentioningEran = new AudioMessage("s1","r1","eran is holding a flower".getBytes());
        AudioMessage messageNotMentioningEran = new AudioMessage("s2","r2","yaniv is holding a flower".getBytes());
        assertEquals(asList("s1","r1"), suggester.suggest("eran", asList(messageMentioningEran, messageNotMentioningEran)));
    }


}
