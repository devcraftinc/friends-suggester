package domain;

public class TextMessage extends Message {

	private final String text;

	public TextMessage(String sender, String receiver, String text) {
		super(sender, receiver);
		this.text = text;
	}

	public String getText() {
		return text;
	}

}