package domain;

import java.util.ArrayList;
import java.util.List;

public class FriendSuggester {
	public List<String> suggest(String user, List<Message> messages) {

		List<String> result = new ArrayList<>();
		
		for (Message m : messages) {
			if (m instanceof TextMessage) {
				TextMessage t = ((TextMessage) m);
				if (t.getText().contains(user)) {
					result.add(t.getSender());
					result.add(t.getReceiver());
				}
			} else if (m instanceof PhotoMessage) {
				PhotoMessage p = ((PhotoMessage) m);
				if (p.getCaption().contains(user)) {
					result.add(p.getSender());
					result.add(p.getReceiver());
				}
			} else if (m instanceof AudioMessage) {
				AudioMessage a = ((AudioMessage) m);
				if (audioToText(a).contains(user)) {
					result.add(a.getSender());
					result.add(a.getReceiver());
				}
			}
		}
		return result;
	}

	private String audioToText(AudioMessage a) {
		return new String(a.getAudio());
	}

}
