package domain;

public  class AudioMessage extends Message {
    private final byte[] audio;

    public AudioMessage(String sender, String receiver, byte[] audio) {
        super(sender, receiver);
        this.audio = audio;
    }

    public byte[] getAudio() {
        return audio;
    }
}
