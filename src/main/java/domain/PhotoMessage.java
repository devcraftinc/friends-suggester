package domain;

public class PhotoMessage extends Message {
    private final String caption;
     private final byte[] photo;

    public PhotoMessage(String sender, String receiver, String caption, byte[] photo) {
        super(sender, receiver);
        this.caption = caption;
        this.photo = photo;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public String getCaption() {
        return caption;
    }

}
